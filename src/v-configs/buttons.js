export const depressed = {
  depressed: true,
  color: 'orange', // TODO: investigate primary light/dark weirdness
  class: 'white--text',
};

export const outlined = {
  outlined: true,
  color: 'primary',
};

export const text = {
  text: true,
  color: 'primary',
  class: 'text-uppercase',
};