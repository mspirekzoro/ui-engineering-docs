import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import VueClipboards from 'vue-clipboards';

Vue.use(Vuetify);
Vue.use(VueClipboards);

export default new Vuetify({
  icons: {
    iconfont: 'fa4',
  },
  theme: {
    themes: {
      light: {
        primary: '#2071a7',
        secondary: '#d24600',
        error: '#dc2a2a',
        success: '#428503',
        warning: '#ffa81b',
        info: '#08457e',
      },
    },
  },
});

// TODO: Use map to define colors for utilites and map states to vuetify defaults
// https://bitbucket.org/zorotools/zoro/pull-requests/6702/global-vuetify-changes
