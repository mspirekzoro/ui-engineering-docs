import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Blog from '@/views/Blog.vue';
import Resources from '@/views/Resources.vue';
import Status from '@/views/Status.vue';
import Alerts from '@/views/Alerts.vue';
import Buttons from '@/views/Buttons.vue';
import Cards from '@/views/Cards.vue';
import Chips from '@/views/Chips.vue';
import Components from '@/views/Components.vue';
import DatePicker from '@/views/DatePicker.vue';
import Spacing from '@/views/Spacing.vue';
import Dialogs from '@/views/Dialogs.vue';
import ExpansionPanels from '@/views/ExpansionPanels.vue';
import Forms from '@/views/Forms.vue';
import Icons from '@/views/Icons.vue';
import NavDrawer from '@/views/NavDrawer.vue';
import Page from '@/views/Page.vue';
import SkeletonLoader from '@/views/SkeletonLoader.vue';
import Typography from '@/views/Typography.vue';
import Colors from '@/views/Colors.vue';
import Elevation from '@/views/Elevation.vue';
import Glossary from '@/views/Glossary.vue';
import Overrides from '@/views/Overrides.vue';
import Tooltip from '@/views/Tooltip.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/blog',
    name: 'Blog',
    component: Blog,
  },
  {
    path: '/spacing',
    name: 'Spacing',
    component: Spacing,
  },
  {
    path: '/overrides',
    name: 'Overrides',
    component: Overrides,
  },
  {
    path: '/Glossary',
    name: 'Glossary',
    component: Glossary,
  },
  {
    path: '/elevation',
    name: 'Elevation',
    component: Elevation,
  },
  {
    path: '/colors',
    name: 'Colors',
    component: Colors,
  },
  {
    path: '/resources',
    name: 'Resources',
    component: Resources,
  },
  {
    path: '/status',
    name: 'Status',
    component: Status,
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/alerts',
    name: 'Alerts',
    component: Alerts,
  },
  {
    path: '/buttons',
    name: 'Buttons',
    component: Buttons,
  },
  {
    path: '/cards',
    name: 'Cards',
    component: Cards,
  },
  {
    path: '/chips',
    name: 'Chips',
    component: Chips,
  },
  {
    path: '/components',
    name: 'Components',
    component: Components,
  },
  {
    path: '/date-picker',
    name: 'DatePicker',
    component: DatePicker,
  },
  {
    path: '/dialogs',
    name: 'Dialogs',
    component: Dialogs,
  },
  {
    path: '/expansion-panels',
    name: 'ExpansionPanels',
    component: ExpansionPanels,
  },
  {
    path: '/forms',
    name: 'Forms',
    component: Forms,
  },
  {
    path: '/icons',
    name: 'Icons',
    component: Icons,
  },
  {
    path: '/nav-drawers',
    name: 'NavDrawers',
    component: NavDrawer,
  },
  {
    path: '/page',
    name: 'Page',
    component: Page,
  },
  {
    path: '/skeleton-loader',
    name: 'SkeletonLoader',
    component: SkeletonLoader,
  },
  {
    path: '/typography',
    name: 'Typography',
    component: Typography,
  },
  {
    path: '/tooltip',
    name: 'Tooltip',
    component: Tooltip,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
